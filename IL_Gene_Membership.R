# Script to associate genes with particular ILs
# Julin Maloof

library(rtracklayer)
library(IRanges)
library(stringr)

#unfortunately there are errors in the ITAG2.3 file so I can't use import.gff3()
genes <- read.delim("../ITAG2.3_gene_models.gff3",sep="\t",comment.char="#",as.is=T,header=F) #Can be downloaded from SGN
#ftp://ftp.sgn.cornell.edu/genomes/Solanum_lycopersicum/annotation/ITAG2.3_release/ITAG2.3_gene_models.gff3

head(genes)
colnames(genes) <- c("seqid","source","type","start","end","score","strand","phase","attributes")
head(genes)

genes <- genes[genes$type=="gene",]

genes$locus <- regmatches(genes$attributes,regexpr("Solyc[01][0-9]g[0-9]{6}",genes$attributes))

head(genes)

genes.rd <- RangedData(IRanges(start=genes$start, end=genes$end, names=genes$locus),space=genes$seqid)

head(genes.rd)

ILs <- read.csv("ChitwoodBoundaries/_02Table.csv",as.is=T)

head(ILs)

#convert the names into R friendly format
ILs$IL2 <- make.names(ILs$IL)
#convert chromosome name to match
ILs$chr <- paste("SL2.40ch",str_pad(ILs$chr,2,pad="0"),sep="")
head(ILs)

#make it a ranged data object

IL.rd <- RangedData(IRanges(start=ILs$PEN..first.,end=ILs$PEN..last.,names=ILs$IL2),space=ILs$chr)
IL.rd

overlaps <- as.data.frame(as.matrix(findOverlaps(ranges(IL.rd),ranges(genes.rd))))

head(overlaps)

overlaps$IL <- ILs$IL2[overlaps$queryHits]
overlaps$locus <- genes$locus[overlaps$subjectHits]

#fix names for IL
overlaps$IL2 <- sub("\\.\\..*$","",overlaps$IL)
unique(overlaps$IL2)

#convert to list
overlaps.l <- sapply(X=unique(overlaps$IL2),FUN=function(x) overlaps[overlaps$IL2==x,"locus"])
maxg <- max(sapply(overlaps.l,length))

#and now to a table
overlaps.m <- sapply(overlaps.l,function(x) c(x,rep(NA,maxg-length(x))))

head(overlaps.m)

write.csv(overlaps.m,"IL_gene_table.csv")









