#convert Aashish RPM file to RPKM for Nick Provart's eFP browser

data <- read.csv("IL_average_normalized_counts_rpm.csv",row.names=1)

head(data[,1:10])

library(Biostrings)
ITAGs <- readDNAStringSet("SLY.swap.fa")
ITAG.length <- nchar(ITAGs)
names(ITAG.length) <- substr(names(ITAGs),1,18)
head(ITAG.length)
#ITAG.length <- data.frame(length=ITAG.length)
#head(ITAG.length)
rownames(data)[!rownames(data) %in% names(ITAG.length)] #all good

#Add zeros for genes with counts below threshold.
zero_count_genes <- names(ITAG.length)[!names(ITAG.length) %in% rownames(data)]

#Trim ITAG.length to only have genes for which we have counts
ITAG.length <- na.omit(ITAG.length[match(rownames(data),names(ITAG.length))])

#checks
all(names(ITAG.length)==rownames(data))
length(ITAG.length) + length(zero_count_genes) == length(ITAGs)

#testing matrix division
m <- matrix(seq(10,90,10),ncol=3)
m
v <- c(10,5,2)
m/v #works!

#calculate RPKM
rpkm <- data / (ITAG.length/1000)
round(head(rpkm[1:5]))
round(head(data[1:5]))
head(ITAG.length)

#append zero_count_genes
zero_count_matrix <- matrix(0,
                            nrow=length(zero_count_genes), 
                            ncol=ncol(rpkm),
                            dimnames=list(zero_count_genes,colnames(rpkm))
)

head(zero_count_matrix)
head(rpkm)
rpkm_all <- rbind(rpkm,zero_count_matrix)
write.csv(rpkm,"IL_rpkm.csv")
write.csv(rpkm_all,"IL_rpkm_all.csv")
